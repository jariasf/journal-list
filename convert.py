#!/usr/bin/env python3

import yaml
import argparse

# Instantiate the parser
parser = argparse.ArgumentParser(description='Conversion tool from yaml to bibstrings')

# Required positional argument
parser.add_argument('files', type=str, nargs='+', help='List of files to process: file1.yaml file2.yml file3.yaml')
parser.add_argument('-f', '--full', help="Name of the output file for full references.", default="full.bib")
parser.add_argument('-a', '--abrv', help="Name of the output file for abreviated references.", default="abrv.bib")

args = parser.parse_args()
files = args.files
full_name = args.full
abrv_name = args.abrv

with open(full_name, 'w') as full, open(abrv_name, 'w') as abrv:
  for file in files:
    with open(file, 'r') as stream:
      try:
        data = yaml.load(stream)
        for record in data:
          full.write('@STRING{{{: <20}= \"{}\"}}\n'.format( record['key'], record['full']) )
          abrv.write('@STRING{{{: <20}= \"{}\"}}\n'.format( record['key'], record['abrv']) )
      except yaml.YAMLError as exc:
        print(exc)